# Changelog

## 4.3.0 - 2025-11-27

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- spip/spip#5973 Invalider le cache (même pour les bots) lorsqu’une URL permanente est ajoutée
- spip/spip#5460 Utiliser les propriétés logiques dans la CSS de l'espace privé

### Removed

- !4853: Supression de _DIR_RESTREINT_ABS
